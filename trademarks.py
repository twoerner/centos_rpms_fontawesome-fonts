#! /usr/bin/python

# Small and dirty script to create the README-Trademarks.txt file. This file
# has to be created by scratch at every release. To do so, use:
# ./trademarks.py > README-Trademarks.txt

import urllib2
import yaml

response = urllib2.urlopen('https://raw.githubusercontent.com/FortAwesome/Font-Awesome/master/src/icons.yml')
document = yaml.load(response)

brands = []

for icon in document['icons']:
    if 'Brand Icons' in icon['categories']:
        brands.append(icon['id'])

brands.sort()

out = 'Brand icons may be subject to trademark and brand guidelines of their\n'
out+= 'respective owners. Always check before deploying other companies\' branding.\n'
out+= '\n'
out+= 'Brand Icons:'

for brand in brands:
    out+= '\n * fa-' + brand

print out
